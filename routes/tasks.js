const express = require('express');
const router = express.Router();
const mongojs = require('mongojs');
var db = mongojs('mongodb://admin:z200135594@ds141024.mlab.com:41024/mytasklist_aviad', ['tasks']);

//get all tasks
router.get('/tasks', (req, res, next) => {
    db.tasks.find((error, tasks) => {
        if(error){
            res.send(error);
        }
         
        res.json(tasks);
    })
});

//get single task
router.get('/task/:id', (req, res, next) => {
    db.tasks.findOne({_id: mongojs.ObjectId(req.params.id)}, (error, task) => {
        if(error){
            res.send(error);
        }
         
        res.json(task);
    })
});

//save task
router.post('/task', (req, res, next) => {
    let task = req.body;
    if(!task.title || (task.isDone + '')) {
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    } else {
        db.tasks.save(task, (error, task) => {
            if(error) {
                res.send(error);
            }
            res.json(task);
        });
    }
});

//delete task 
router.delete('/task/:id', (req, res, next) => {
    db.tasks.remove({_id: mongojs.ObjectId(req.params.id)}, (error, task) => {
        if(error){
            res.send(error);
        }
         
        res.json(task);
    })
});

//update task
router.put('/task/:id', (req, res, next) => {
    let task = req.body;
    let updateTask = {};

    if(task.isDone) {
        updateTask.isDone = task.isDone;
    }

    if(task.title) {
        updateTask.title = task.title;
    }

    if(!updateTask) {
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    } else {
        db.tasks.update({_id: mongojs.ObjectId(req.params.id)}, updateTask, (error, task) => {
            if(error){
                res.send(error);
            }
             
            res.json(task);
        });
    }

    
});

module.exports = router;