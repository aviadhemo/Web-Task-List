const express = require('express');
const path = require('path');
const bodyPrser = require('body-parser');

const index = require('./routes/index');
const tasks = require('./routes/tasks');

const port = 3000;

var app = express();

//view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view-engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//set static folder
app.use(express.static(path.join(__dirname, 'client')));

//body parser middleware
app.use(bodyPrser.json());
app.use(bodyPrser.urlencoded({ extended: false}));

app.use('/', index);
app.use('/api', tasks);

app.listen(port, () => {
    console.log('server starter on port ', port);
});

